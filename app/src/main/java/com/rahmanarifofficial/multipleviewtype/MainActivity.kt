package com.rahmanarifofficial.multipleviewtype

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val menuData = Menu.menus

        val menuAdapter = MenuAdapter(menuData)

        rv_menu_item.hasFixedSize()
        rv_menu_item.layoutManager = LinearLayoutManager(this)
        rv_menu_item.adapter = menuAdapter
    }
}