package com.rahmanarifofficial.multipleviewtype

data class MenuItem(
    val name: String,
    val price: Int,
    var count: Int,
    val itemPreview: Int,
    val flag: Int
)