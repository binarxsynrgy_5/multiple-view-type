package com.rahmanarifofficial.multipleviewtype

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_menu.view.*
import kotlinx.android.synthetic.main.item_menu_header.view.*


class MenuAdapter(private val data: List<Any>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val ITEM_HEADER = 0
        private const val ITEM_MENU = 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (data[position]) {
            is String -> ITEM_HEADER
            is MenuItem -> ITEM_MENU
            else -> throw IllegalArgumentException("Undefined view type")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_HEADER -> MenuHeaderHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_menu_header, parent, false))
            ITEM_MENU -> MenuItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false))
            else -> throw throw IllegalArgumentException("Undefined view type")
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            ITEM_HEADER -> {
                val headerHolder = holder as MenuHeaderHolder
                headerHolder.bindContent(data[position] as String)
            }
            ITEM_MENU -> {
                val itemHolder = holder as MenuItemHolder
                itemHolder.bindContent(data[position] as MenuItem)
            }
            else -> throw IllegalArgumentException("Undefined view type")
        }
    }

    override fun getItemCount() = data.size
}

class MenuHeaderHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bindContent(text: String) {
        itemView.text_header.text = text
    }
}

class MenuItemHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer  {

    fun bindContent(menuItem: MenuItem) {
        itemView.item_count.text = menuItem.count.toString()
        itemView.item_name.text = menuItem.name
        val price = "Rp. ${menuItem.price}"
        itemView.item_price.text = price
    }
}
